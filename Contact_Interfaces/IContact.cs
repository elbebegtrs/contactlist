﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contact_Interfaces
{
    public interface IContact
    {
        int Id { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        List<IAddress> Address { get; set; }
    }
}
