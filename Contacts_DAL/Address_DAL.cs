﻿using Contacts_Domain.Models;
using Contacts_Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contacts_DAL
{
    public class Address_DAL
    {
        public static Address Get(int id)
        {
            using (var context = new ContactContext())
            {
                var contact_Address = context.Address.FirstOrDefault(x => x.Id == id);

                if (contact_Address != null)
                    return contact_Address;

                return null;
            }
        }

        public static Address Get(Contact contact)
        {
            using (var context = new ContactContext())
            {
                var contact_Address = context.Address.FirstOrDefault(x => x.Contact.Id == contact.Id);

                if (contact_Address != null)
                    return contact_Address;

                return null;
            }
        }


        public static List<Address> GetAll(int id)
        {
            using (var context = new ContactContext())
            {
                var contact_Address = context.Address.Where(x => x.Contact.Id == id).ToList();

                return contact_Address;
            }
        }

        public static List<Address> GetAll(Contact contact)
        {
            using (var context = new ContactContext())
            {
                var contact_Address = context.Address.Where(x=> x.Contact.Id == contact.Id).ToList();

                return contact_Address;
            }
        }

        public static void Insert(Address newAddress)
        {
            using (var context = new ContactContext())
            {
                context.Address.Add(newAddress);

                context.SaveChanges();
            }
        }

        public static void Remove(Address newAddress)
        {
            using (var context = new ContactContext())
            {
                //var recordToRemove = context.Address.Where(x =>
                //x.FirstName == newContact.FirstName &&
                //x.LastName == newContact.LastName).FirstOrDefault();

                //if (recordToRemove != null)
                //{
                //    context.Contact.Remove(recordToRemove);
                //    context.SaveChanges();
                //}
            }
        }

        public static void Update(Address newAddress)
        {
            using (var context = new ContactContext())
            {
                var recordToUpdate = context.Address.FirstOrDefault(c => c.Street == newAddress.Street && c.City == newAddress.City && c.State == newAddress.State && c.Zip == newAddress.Zip && c.Contact.Id == newAddress.Contact.Id);

                if (recordToUpdate != null)
                {
                    recordToUpdate.Contact = newAddress.Contact;
                    recordToUpdate.Street = newAddress.Street;
                    recordToUpdate.City = newAddress.City;
                    recordToUpdate.State = newAddress.State;
                    recordToUpdate.Zip = newAddress.Zip;

                    context.Entry(recordToUpdate).State = System.Data.Entity.EntityState.Modified;

                    context.SaveChanges();
                }
            }
        }

        public static bool DoesAddressExists(Address addressToCheck)
        {
            using (var context = new ContactContext())
            {
                return context.Address.Any(x=> x.Street == addressToCheck.Street && x.City == addressToCheck.City 
                && x.State == addressToCheck.State && x.Zip == addressToCheck.Zip && x.Contact == addressToCheck.Contact);
            }
        }
    }
}
