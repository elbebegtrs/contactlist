﻿using Contacts_Domain.Models;
using Contacts_Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contacts_DAL
{
    public class Contact_DAL
    {

        public static Contact Get(int id)
        {
            using (var context = new ContactContext())
            {
                var contact = context.Contact.FirstOrDefault(x => x.Id == id);

                if (contact != null)
                    return contact;

                return null;
            }
        }

        public static List<Contact> GetAll()
        {
            using (var context = new ContactContext())
            {
                var contacts = context.Contact.ToList();

                return contacts;
            }
        }

        public static void Insert(Contact newContact)
        {
            using (var context = new ContactContext())
            {
                context.Contact.Add(newContact);
                context.SaveChanges();
            }
        }

        public static void Remove(Contact newContact)
        {
            using (var context = new ContactContext())
            {
                var recordToRemove = context.Contact.Where(x =>
                x.FirstName == newContact.FirstName &&
                x.LastName == newContact.LastName).FirstOrDefault();

                if (recordToRemove != null)
                {
                    context.Contact.Remove(recordToRemove);
                    context.SaveChanges();
                }                
            }
        }

        public static int Update(Contact newContact)
        {
            using (var context = new ContactContext())
            {
                var recordToUpdate = context.Contact.FirstOrDefault(c=>c.Id == newContact.Id);

                if (recordToUpdate != null)
                {
                    recordToUpdate.FirstName = newContact.FirstName;
                    recordToUpdate.LastName = newContact.LastName;

                    context.Entry(recordToUpdate).State = System.Data.Entity.EntityState.Modified;

                    context.SaveChanges();

                    return newContact.Id;
                }

                return 0;
            }
        }
    }
}
