﻿using Contact_Interfaces;
using Contacts_Business.Adapters;
using Contacts_Business.Helpers;
using Contacts_Business.Models;
using Contacts_DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contacts_Business.Managers
{
    public class Contact_Manager
    {
        public static List<Contact> InitList()
        {
            return Contact_DAL.GetAll().Select(x => Contact__Manager_Helper.GetFullContact(x)).ToList();
        }

        public static bool SaveNewContact(Contact contact)
        {
            try
            {
                //Contact_DAL.Insert(Contact_Adapter.ModelToDomain(contact));

                var newDomainList = contact.Address.Select(x => Address_Adapter.ModelToDomain(x)).ToList();

                foreach (var domainObject in newDomainList)
                {
                    if(!Address_DAL.DoesAddressExists(domainObject))
                    {
                        domainObject.Contact = Contact_Adapter.ModelToDomain(contact);

                        Address_DAL.Insert(domainObject);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool UpdateContact(Contact contact)
        {
            try
            {
                var domainContact = Contact_Adapter.ModelToDomain(contact);

                 Contact_DAL.Update(domainContact);

                foreach (var contactAddress in contact.Address)
                {
                    var addressDomain = Address_Adapter.ModelToDomain(contactAddress);

                    addressDomain.Contact = domainContact;

                    Address_DAL.Update(addressDomain);
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static Contact GetContact(int id)
        {
            var domainContact = Contact_DAL.Get(id);

            var contact = Contact_Adapter.DomainToModel(domainContact);

            contact.Address = Address_DAL.GetAll(domainContact).Select(x => Address_Adapter.DomainToModel(x)).ToList();

            return contact;
        }
    }
}
