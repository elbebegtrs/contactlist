﻿using Contact_Interfaces;
using Contacts_Business.Adapters;
using Contacts_DAL;
using Contacts_Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contacts_Business.Helpers
{
    public class Contact__Manager_Helper
    {
        public static Models.Contact GetFullContact(Contact contact)
        {
            Models.Contact returnContact = Contact_Adapter.DomainToModel(contact);

            returnContact.Address = Address_DAL.GetAll(contact.Id).Select(x => Address_Adapter.DomainToModel(x)).ToList();

            return returnContact;
        }
    }
}
