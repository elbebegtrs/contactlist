﻿using Contact_Interfaces;
using Contacts_Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contacts_Business.Adapters
{
    public class Address_Adapter
    {
        public static Models.Address DomainToModel(Address oldAddress)
        {
            return new Models.Address()
            {
                Id = oldAddress.Id,
                Street = oldAddress.Street,
                City = oldAddress.City,
                State= oldAddress.State,
                Zip = oldAddress.Zip
            };
        }

        public static Address ModelToDomain(Models.Address oldAddress)
        {
            return new Address()
            {
                Id = oldAddress.Id,
                Street = oldAddress.Street,
                City = oldAddress.City,
                State = oldAddress.State,
                Zip = oldAddress.Zip
            };
        }
    }
}
