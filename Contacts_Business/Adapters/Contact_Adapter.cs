﻿using Contact_Interfaces;
using Contacts_Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contacts_Business.Adapters
{
    public class Contact_Adapter
    {
        public static Models.Contact DomainToModel(Contact oldContact)
        {
            return new Models.Contact()
            {
                Id = oldContact.Id,
                FirstName = oldContact.FirstName,
                LastName = oldContact.LastName
            };
        }

        public static Contact ModelToDomain(Models.Contact oldContact)
        {
            return new Contact()
            {
                Id = oldContact.Id,
                FirstName = oldContact.FirstName,
                LastName = oldContact.LastName
            };
        }
        
    }
}
