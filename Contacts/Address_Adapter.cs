﻿using Contacts_Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contacts
{
    public class Address_Adapter
    {
        public static Models.Contact_Address BusinessModelToContactModel(Address oldAddress)
        {
            return new Models.Contact_Address()
            {
                Id = oldAddress.Id,
                Street = oldAddress.Street,
                City = oldAddress.City,
                State = oldAddress.State,
                Zip = oldAddress.Zip
            };
        }

        public static Address ContactModelToBusinessModel(Models.Contact_Address oldAddress)
        {
            return new Address()
            {
                Id = oldAddress.Id,
                Street = oldAddress.Street,
                City = oldAddress.City,
                State = oldAddress.State,
                Zip = oldAddress.Zip 
            };
        }
    }
}