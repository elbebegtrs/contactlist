﻿using Contacts_Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contacts
{
    public class Contact_Adapter
    {
        public static Models.Contact BusinessModelToContactModel(Contact oldContact)
        {
            return new Models.Contact()
            {
                Id = oldContact.Id,
                FirstName = oldContact.FirstName,
                LastName = oldContact.LastName,
                Address = oldContact.Address.Select(x=> Address_Adapter.BusinessModelToContactModel(x)).ToList()
            };
        }

        public static Contact ContactModelToBusinessModel(Models.Contact oldContact)
        {
            return new Contact()
            {
                Id = oldContact.Id,
                FirstName = oldContact.FirstName,
                LastName = oldContact.LastName,
                Address = oldContact.Address.Select(x => Address_Adapter.ContactModelToBusinessModel(x)).ToList()
            };
        }
    }
}