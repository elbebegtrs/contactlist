﻿using Contact_Interfaces;
using Contacts.Models;
using Contacts_Business.Managers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Contacts.Controllers
{
    public class ContactController : ApiController
    {
        [HttpPost]
        [Route("api/contact")]
        public IHttpActionResult Insert([FromBody]Contact contact)
        {
            return Ok(Contact_Manager.SaveNewContact(Contact_Adapter.ContactModelToBusinessModel(contact)));
        }

        [HttpPost]
        [Route("api/contact/update")]
        public IHttpActionResult Updte([FromBody]Contact contact)
        {
            return Ok(Contact_Manager.UpdateContact(Contact_Adapter.ContactModelToBusinessModel(contact)));
        }


        [HttpGet]
        [Route("api/contact/{id}")]
        public IHttpActionResult GetContact(int id)
        {
            return Ok(Contact_Manager.GetContact(id));
        }
    }
}
