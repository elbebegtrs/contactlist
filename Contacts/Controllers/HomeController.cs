﻿using Contact_Interfaces;
using Contacts_Business.Managers;
using Contacts_Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Contacts.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            List<Contact> contacts = Contact_Manager.InitList();

            return View(contacts);
        }
    }
}