namespace Contacts_Infrastructure.Migrations
{
    using Contacts_Domain.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Contacts_Infrastructure.ContactContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(Contacts_Infrastructure.ContactContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            //Contact newContact = new Contact()
            //{
            //    FirstName = "Andres",
            //    LastName = "Camargo"
            //};

            //Contact newContact2 = new Contact()
            //{
            //    FirstName = "Brandon",
            //    LastName = "Camargo"
            //};

            //Address newAddress = new Address()
            //{
            //    Contact = newContact,
            //    Street = "202 some Street",
            //    City = "Brentwood",
            //    State = "NY",
            //    Zip = "11717"
            //};

            //Address newAddress2 = new Address()
            //{
            //    Contact = newContact,
            //    Street = "322200 anoyjet Street",
            //    City = "SmitjhTown",
            //    State = "NY",
            //    Zip = "11787"
            //};

            //Address newAddress3 = new Address()
            //{
            //    Contact = newContact2,
            //    Street = "322200 yet some Street",
            //    City = "ridge",
            //    State = "NY",
            //    Zip = "11221"
            //};

            //context.Address.Add(newAddress);
            //context.Address.Add(newAddress2);
            //context.Address.Add(newAddress3);

            //context.SaveChanges();
        }
    }
}
