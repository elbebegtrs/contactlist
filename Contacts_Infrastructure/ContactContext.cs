﻿using Contacts_Domain.Models;
using Contacts_Infrastructure.Migrations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contacts_Infrastructure
{
    public class ContactContext : DbContext
    {
        public ContactContext() : base("Test")
        {
            //code first migration
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ContactContext, Configuration>());
        }

        public DbSet<Contact> Contact { get; set; }
        public DbSet<Address> Address { get; set; }
    }
}
